DVIPS := dvips
LATEX := latex
#PDFLATEX := pdflatex
PDFLATEX := xelatex
BIBTEX := bibtex
PROJECT := Jerabek-thesis-2019-canfd

LATEXFLAGS := -shell-escape

#FIG_DITAA_TXT := $(wildcard figures/*.txt)
#FIG_DITAA := $(FIG_DITAA_TXT:.txt=.pdf)

FIG_WAVE_JS := $(wildcard figures/*.js)
FIG_WAVE := $(FIG_WAVE_JS:.js=.pdf)

FIG_JPG := $(wildcard figures/*.jpg)
FIG_JPG_PDF := $(FIG_JPG:.jpg=.pdf)

FIG_GIF := $(wildcard figures/*.gif)
FIG_GIF_PDF := $(FIG_GIF:.gif=.pdf)

FIG_SVG := $(wildcard figures/*.svg)
#figures/can-schema.svg
FIG_SVG_PDF := $(FIG_SVG:.svg=.pdf)

FIG_DOT := $(wildcard figures/*.dot)
FIG_DOT_PDF := $(FIG_DOT:.dot=.pdf)

.PHONY: all clean

all: $(PROJECT).pdf prezentace/prezentace.pdf

prezentace/prezentace.pdf: $(wildcard prezentace/*.tex *.tex *.bib) $(FIG_DITAA) $(FIG_WAVE) $(FIG_JPG_PDF) $(wildcard appendices/*.tex) $(FIG_GIF_PDF) $(FIG_SVG_PDF)
	cd prezentace && $(PDFLATEX) prezentace
	cd prezentace && $(PDFLATEX) prezentace

$(PROJECT).pdf: $(wildcard *.tex *.bib) $(FIG_DITAA) $(FIG_WAVE) $(FIG_JPG_PDF) $(wildcard appendices/*.tex) $(FIG_GIF_PDF) $(FIG_DOT_PDF) $(FIG_SVG_PDF)
	$(PDFLATEX) $(LATEXFLAGS) $(PROJECT).tex
	makeindex $(PROJECT).nlo -s nomencl.ist -o $(PROJECT).nls
	$(PDFLATEX) $(LATEXFLAGS) $(PROJECT).tex
	$(BIBTEX) $(PROJECT)
	$(PDFLATEX) $(LATEXFLAGS) $(PROJECT).tex
	$(PDFLATEX) $(LATEXFLAGS) $(PROJECT).tex
FORCE:
clean:
	for d in . prezentace; do (cd $$d && rm -f *.log *.aux *.bbl *.blg *.lof *.lot *.dvi *.toc *.out *~ *.ps *.ilg *.nlo *.nls *.lol *.brf *.cpt); done

figures/%.pdf: figures/%.png
	#potrace -b pdf -o $@ $<
	convert $< $@
figures/%.bmp: figures/%.png
	convert $< $@
figures/%.png: figures/%.txt
	#ditaa -S -A -s 5 -o -r $< $@
	ditaa -r -o $< $@

figures/%.pdf: figures/%.svg
	inkscape --export-pdf=$@ $<
figures/%.svg: figures/%.js
	wavedrom-cli -i $< -s $@

figures/%.pdf: figures/%.dot
	dot -Tpdf $< >$@

figures/%.pdf: figures/%.jpg
	convert $< $@
figures/%.pdf: figures/%.gif
	convert $< $@
figures/filelist.pdf:

$(FIG_TESTS_PDF:.pdf=%pdf): $(FIG_TESTS_JSONS)
	cd figures/tests && python plot.py
