//#define _GNU_SOURCE
#include <ctype.h>
#include <errno.h>
#include <error.h>
#include <fcntl.h>
#include <math.h>
#include <net/if.h>
#include <poll.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/net_tstamp.h>

static inline int sock_get_if_index(int s, const char *if_name)
{
    struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));

    strcpy(ifr.ifr_name, if_name);
    if (ioctl(s, SIOCGIFINDEX, &ifr) < 0)
        error(1, errno, "SIOCGIFINDEX '%s'", if_name);
    return ifr.ifr_ifindex;
}

void set_sched_policy_and_prio(int policy, int rtprio)
{
    struct sched_param scheduling_parameters;
    int maxprio=sched_get_priority_max(policy);
    int minprio=sched_get_priority_min(policy);

    if((rtprio < minprio) || (rtprio > maxprio))
        error(1, 0, "The priority for requested policy is out of <%d, %d> range\n",
              minprio, maxprio);

    scheduling_parameters.sched_priority = rtprio;

    if (0 != pthread_setschedparam(pthread_self(), policy, &scheduling_parameters))
        error(1, errno, "pthread_setschedparam error");
}

void set_sndbuf(int sock, int size) {
    socklen_t optlen;
    int sendbuff;
    int res;

    // Get buffer size
    optlen = sizeof(sendbuff);
    res = getsockopt(sock, SOL_SOCKET, SO_SNDBUF, &sendbuff, &optlen);

    if(res == -1)
        error(1, errno, "Error getsockopt one");
    else
        printf("send buffer size = %d\n", sendbuff);

    // Set buffer size
    sendbuff = size;

    printf("sets the send buffer to %d\n", sendbuff);
    res = setsockopt(sock, SOL_SOCKET, SO_SNDBUF, &sendbuff, sizeof(sendbuff));

    if(res == -1)
        error(1, errno, "Error setsockopt");
}

int main(int argc, char *argv[])
{
    int s;
    struct sockaddr_can addr;
    struct canfd_frame frame;
    int frame_size = sizeof(struct can_frame);

    if (argc <= 2) {
        printf("Usage: %s can_ifc can_id\n", argv[0]);
        return 1;
    }

    const char *ifc = argv[1];
    int can_id = strtoul(argv[2], nullptr, 0) & 0x3ff;
    bool fd_mode = false;
    if (argc > 3 && strcmp(argv[3], "-f") == 0)
        fd_mode = true;

    printf("Using interface %s, can id 0x%03x, fd mode %s\n", ifc, can_id, fd_mode ? "on" : "off");

    memset(&frame, 0, sizeof(frame));
    frame.can_id = can_id;
    frame.len = 4;
    frame.data[0] = 0xaa;
    frame.data[1] = 0x55;
    frame.data[2] = 0xff;
    frame.data[3] = 0x00;

    if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
        error(1, errno, "socket");

    addr.can_family = AF_CAN;
    addr.can_ifindex = sock_get_if_index(s, ifc);

    if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        error(1, errno, "bind");

    if (fd_mode) {
        int enable_canfd = 1;
        if (setsockopt(s, SOL_CAN_RAW, CAN_RAW_FD_FRAMES,
                       &enable_canfd, sizeof(enable_canfd))){
            printf("error when enabling CAN FD support\n");
            return 1;
        }
        frame.data[3] = 0xaa;
        frame.data[2] = 0x55;
        frame.data[1] = 0xff;
        frame.data[0] = 0x00;
        frame.flags |= CANFD_BRS;
        frame_size = sizeof(struct canfd_frame);
    }

    set_sndbuf(s, frame_size*4);

    set_sched_policy_and_prio(SCHED_FIFO, 40);
    int res;
    for (;;) {
        res = write(s, &frame, frame_size);
        if (errno == 0) {

        } else if (errno == ENOBUFS) {
            printf("delay\n");
            //usleep(10);
        } else {
            perror("write");
            break;
        }
    }

    close(s);
    return 0;
}
