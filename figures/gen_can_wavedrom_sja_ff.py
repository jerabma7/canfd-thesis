#!/usr/bin/python3
from itertools import chain, repeat
from string import Template
from math import ceil
from vcd.writer import VCDWriter
flatten = chain.from_iterable

# nominal br: 16 tq / bit, st 13/16 (81.25%)
# data br   : 11 tq / bit, st 9/11 (81.8%)

nsp = 13
ntq = 16

dsp = 9
dtq = 11


def norm(x):
    return ''.join(c if c != p else '.' for c, p in zip(x, chain([None], x)))


def nom(bits: str) -> str:
    """Convert sequence in bits to sequence in time quanta."""
    return ''.join(flatten(repeat(c, ntq) for c in bits))


def dat(bits: str) -> str:
    """Convert sequence in bits to sequence in time quanta."""
    return ''.join(flatten(repeat(c, dtq) for c in bits))


def mask(a, mask):
    return ''.join(c if m == '1' else '1' for c, m in zip(a, mask))


# sample points, in time quanta
sp_nom = nsp*'0' + '1' + (ntq-nsp)*'0'
sp_dat = dsp*'0' + '1' + (dtq-dsp)*'0'

RX = dat('111110'*100)  # 5 '1' data bits, followed by stuff bit

# replicate for all the bits
sp_nom *= ceil(len(RX)/dtq)
sp_dat *= ceil(len(RX)/dtq)
sp_nom = sp_nom[:len(RX)]
sp_dat = sp_dat[:len(RX)]

# sampled value in nominal bittime ('1' if not in sample point for better view)
sampled_nom = mask(RX, sp_nom)

with open('sja_ff.vcd', 'wt') as f, VCDWriter(f, timescale='100 ns') as vcd:
    v_can_rx = vcd.register_var('top', 'can_rx', 'wire', size=1, init='1')
    v_sp_nom = vcd.register_var('top', 'sp_nom', 'wire', size=1, init='0')
    v_sp_dat = vcd.register_var('top', 'sp_dat', 'wire', size=1, init='0')
    v_sampled_nom = vcd.register_var('top', 'sampled_nom', 'wire', size=1, init='1')
    vars = [
        (v_can_rx, RX),
        (v_sp_nom, sp_nom),
        (v_sp_dat, sp_dat),
        (v_sampled_nom, sampled_nom),
    ]
    maxt = min(len(d) for _, d in vars)
    vars = [(v, norm(d)) for v, d in vars]
    for t in range(maxt):
        for v, d in vars:
            dd = d[t]
            if dd != '.':
                vcd.change(v, t, dd)


# print(Template("""\
# {signal: [
#     {name: 'can_rx', wave: '$rx'},
#     {name: 'DSP', wave: '$sp_dat'},
#     {name: 'NSP', wave: '$sp_nom'},
#     {name: 'sampled', wave: '$sampled_nom'},
# ]}
# """).substitute(rx=norm(RX), sp_nom=norm(sp_nom), sp_dat=norm(sp_dat), sampled_nom=norm(sampled_nom)))
