\chapter{SocketCAN Driver for CTU CAN FD}
\label{ch:ctucanfd}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{About CTU CAN FD}

CTU CAN FD is an open source soft core written in VHDL. It originated in 2015 as
a Master's project by Ondrej Ille at the Department of Measurement of FEE at
CTU. After a few years, a company became interested in the core and expressed
the desire to be able to synthesize several such cores in FPGA and access them
from Linux via SocketCAN.

This lead to new interest in the project with much higher demand for test
coverage and design reliability. It was necessary to optimize the core and
redesign the register map for easy and safe use from a potentially
multi-processor environment. That was kept on the core's author, with me and my
supervisor as consultants.

It was also necessary to create the SocketCAN driver and integrate the core to
Xilinx Zynq SoC in our MicroZed board for testing. As the core was being
significantly rewritten, there arose the need to have some form of automated
tests to detect errors soon. These tasks were assigned to me, and they are
described in this and the following chapters.

To sum up, the following tasks were necessary to perform on the core (the
italicized ones were performed by someone else and are out of the scope of this
thesis):
\hyphenation{au-to-ma-ti-cal-ly}
\begin{itemize}
\item \textit{Optimize the core (resource usage, maximum operating frequency)}
\item Integrate the core to MicroZed testing platform
\item Create SocketCAN driver
\item Create an automated verification framework and environment to
      automatically run the tests
\end{itemize}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{About SocketCAN}
\label{sec:socketcan}
% TODO: maybe move to chatper 1 - theory and introduction


SocketCAN is a standard common interface for CAN devices in the Linux kernel. As
the name suggests, the bus is accessed via sockets, similarly to common network
devices. The reasoning behind this is in depth described in
\cite{linux:socketcan}. In short, it offers a natural way to implement and work
with higher layer protocols over CAN, in the same way as, e.g., UDP/IP over
Ethernet.

\subsection{Device probe}

Before going into detail about the structure of a CAN bus device driver, let's
reiterate how the kernel gets to know about the device at all. Some buses, like
PCI or PCIe, support device enumeration. That is, when the system boots, it
discovers all the devices on the bus and reads their configuration. The kernel
identifies the device via its vendor ID and device ID, and if there is a driver
registered for this identifier combination, its probe method is invoked to
populate the driver's instance for the given hardware. A similar situation goes
with USB, only it allows for device hot-plug.

The situation is different for peripherals which are directly embedded in the
SoC and connected to an internal system bus (AXI, APB, Avalon, and others).
These buses do not support enumeration, and thus the kernel has to learn about
the devices from elsewhere. This is exactly what the Device Tree was made for.

\subsection{Device tree}

An entry in device tree states that a device exists in the system, how it is
reachable (on which bus it resides) and its configuration -- registers address,
interrupts and so on. An example of such a device tree is given in
\fref{lst:ctucan:devtree}.

\begin{listing}
    \begin{minted}[autogobble]{dts}
        / {
            /* ... */
            amba: amba {
                #address-cells = <1>;
                #size-cells = <1>;
                compatible = "simple-bus";

                CTU_CAN_FD_0: CTU_CAN_FD@43c30000 {
                    compatible = "ctu,ctucanfd";
                    interrupt-parent = <&intc>;
                    interrupts = <0 30 4>;
                    clocks = <&clkc 15>;
                    reg = <0x43c30000 0x10000>;
                };
            };
        };
    \end{minted}
    \caption{An excerpt from device tree declaring an AMBA bus, to which one
             CTU CAN FD core is attached.}
    \label{lst:ctucan:devtree}
\end{listing}

\subsection{Driver structure}
\label{sec:socketcan:drv}

The driver can be divided into two parts -- platform-dependent device discovery
and set up, and platform-independent CAN network device implementation.

\subsubsection{Platform device driver}
\label{sec:socketcan:platdev}

In the case of Zynq, the core is connected via the AXI system bus, which does
not have enumeration support, and the device must be specified in Device Tree.
This kind of devices is called \textit{platform device} in the kernel and is
handled by a \textit{platform device driver}\footnote{Other buses have their own
specific driver interface to set up the device.}.

A platform device driver provides the following things:
\begin{itemize}
\item A \textit{probe} function
\item A \textit{remove} function
\item A table of \textit{compatible} devices that the driver can handle
\end{itemize}

The \textit{probe} function is called exactly once when the device appears (or
the driver is loaded, whichever happens later). If there are more devices
handled by the same driver, the \textit{probe} function is called for each one
of them. Its role is to allocate and initialize resources required for handling
the device, as well as set up low-level functions for the platform-independent
layer, e.g., \textit{read\_reg} and \textit{write\_reg}. After that, the driver
registers the device to a higher layer, in our case as a \textit{network
device}.

The \textit{remove} function is called when the device disappears, or the driver
is about to be unloaded. It serves to free the resources allocated in
\textit{probe} and to unregister the device from higher layers.

Finally, the table of \textit{compatible} devices states which devices the
driver can handle. The Device Tree entry \verb|compatible| is matched against
the tables of all \textit{platform drivers}.

\begin{listing}[ht]
    \begin{minted}[autogobble]{c}
        /* Match table for OF platform binding */
        static const struct of_device_id ctucan_of_match[] = {
            { .compatible = "ctu,canfd-2", },
            { .compatible = "ctu,ctucanfd", },
            { /* end of list */ },
        };
        MODULE_DEVICE_TABLE(of, ctucan_of_match);

        static int ctucan_probe(struct platform_device *pdev);
        static int ctucan_remove(struct platform_device *pdev);

        static struct platform_driver ctucanfd_driver = {
            .probe  = ctucan_probe,
            .remove = ctucan_remove,
            .driver = {
                .name = DRIVER_NAME,
                .of_match_table = ctucan_of_match,
            },
        };
        module_platform_driver(ctucanfd_driver);
    \end{minted}
    \caption[Platform device driver declaration]{Platform device driver
             declaration. Only prototypes of the \textit{probe} and
             \textit{remove} functions are included.}
    \label{fig:platform-driver}
\end{listing}

\subsubsection{Network device driver}
\label{sec:socketcan:netdev}

Each network device must support at least these operations:
\begin{itemize}
\item Bring the device up: \verb|ndo_open|
\item Bring the device down: \verb|ndo_close|
\item Submit TX frames to the device: \verb|ndo_start_xmit|
\item Signal TX completion and errors to the network subsystem: ISR
\item Submit RX frames to the network subsystem: ISR and NAPI
\end{itemize}

There are two possible event sources: the device and the network subsystem.
Device events are usually signaled via an interrupt, handled in an Interrupt
Service Routine (ISR). Handlers for the events originating in the network
subsystem are then specified in \verb|struct net_device_ops|.

When the device is brought up, e.g., by calling \verb|ip link set can0 up|, the
driver's function \verb|ndo_open| is called. It should validate the interface
configuration and configure and enable the device. The analogous opposite is
\verb|ndo_close|, called when the device is being brought down, be it explicitly
or implicitly.

When the system should transmit a frame, it does so by calling
\verb|ndo_start_xmit|, which enqueues the frame into the device. If the device
HW queue (FIFO, mailboxes or whatever the implementation is) becomes full, the
\verb|ndo_start_xmit| implementation informs the network subsystem that it
should stop the TX queue (via \verb|netif_stop_queue|). It is then re-enabled
later in ISR when the device has some space available again and is able to
enqueue another frame.

All the device events are handled in ISR, namely:
\begin{enumerate}
\item \textbf{TX completion}. When the device successfully finishes
      transmitting a frame, the frame is echoed locally. On error, an
      informative error frame\footnote{Not to be mistaken with CAN Error Frame.
      This is a \verb|can_frame| with \verb|CAN_ERR_FLAG| set and some error
      info in its \verb|data| field.} is sent to the network subsystem instead.
      In both cases, the software TX queue is resumed so that more frames may be
      sent.
\item \textbf{Error condition}. If something goes wrong (e.g., the device goes
      bus-off or RX overrun happens), error counters are updated, and
      informative error frames are enqueued to SW RX queue.
\item \textbf{RX buffer not empty}. In this case, read the RX frames and
      enqueue them to SW RX queue. Usually NAPI is used as a middle layer (see
      \fref{sec:socketcan:napi}).
\end{enumerate}

\subsection{NAPI}
\label{sec:socketcan:napi}

The frequency of incoming frames can be high and the overhead to invoke the
interrupt service routine for each frame can cause significant system load.
There are multiple mechanisms in the Linux kernel to deal with this situation.
They evolved over the years of Linux kernel development and enhancements. For
network devices, the current standard is NAPI -- \textit{the New API}. It is
similar to classical top-half/bottom-half interrupt handling in that it only
acknowledges the interrupt in the ISR and signals that the rest of the
processing should be done in softirq context. On top of that, it offers the
possibility to \textit{poll} for new frames for a while. This has a potential to
avoid the costly round of enabling interrupts, handling an incoming IRQ in ISR,
re-enabling the softirq and switching context back to softirq.

More detailed documentation of NAPI may be found on the pages of Linux
Foundation \cite{linux:napi}.
% TODO: add reference to bootlin.com, LDD

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Integrating the core to Xilinx Zynq} % TODO: copyrights, etc. etc.

% Integration:
% - APB
% - component
% - auto builds

The core interfaces a simple subset of the Avalon \cite{intel:avalon} bus as it
was originally used on Alterra FPGA chips, yet Xilinx natively interfaces with
AXI \cite{arm:axi}. The most obvious solution would be to use an Avalon/AXI
bridge or implement some simple conversion entity. However, the core's interface
is half-duplex with no handshake signaling, whereas AXI is full duplex with
two-way signaling. Moreover, even AXI-Lite slave interface is quite
resource-intensive, and the flexibility and speed of AXI are not required for a
CAN core.

% TODO: core's avalon signals, AXI-Lite signals and example of read/write
%       transactions (copy from Bachelor's thesis)

Thus a much simpler bus was chosen -- APB (Advanced Peripheral Bus)
\cite{arm:apb}.
% TODO: description of APB, signals, images
APB-AXI bridge is directly available in Xilinx Vivado, and the interface adaptor
entity is just a few simple combinatorial assignments.

% component
Finally, to be able to include the core in a block diagram as a
custom IP, the core, together with the APB interface, has been packaged as a
Vivado component.

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{CTU CAN FD Driver design} % TODO: better name

The general structure of a CAN device driver has already been examined in
\fref{sec:socketcan:drv}. The next paragraphs provide a more detailed
description of the CTU CAN FD core driver in particular.

\subsection{Low-level driver}

The core is not intended to be used solely with SocketCAN, and thus it is
desirable to have an OS-independent low-level driver. This low-level driver can
then be used in implementations of OS driver or directly either on bare metal or
in a user-space application. Another advantage is that if the hardware slightly
changes, only the low-level driver needs to be modified.

The code\footnote{Available in \path{/driver} in CTU CAN FD repository
\cite{git:ctucanfd}} is in part automatically generated and in part written
manually by the core author, with contributions of the thesis' author. The
low-level driver supports operations such as: set bit timing, set controller
mode, enable/disable, read RX frame, write TX frame, and so on.

\subsection{Configuring bit timing}

On CAN, each bit is divided into four segments: \textsc{SYNC}, \textsc{PROP},
\textsc{PHASE1}, and \textsc{PHASE2}. Their duration is expressed in multiples
of a Time Quantum (details in \cite{spec:can20}, chapter 8). When configuring
bitrate, the durations of all the segments (and time quantum) must be computed
from the bitrate and Sample Point. This is performed independently for both the
Nominal bitrate and Data bitrate for CAN FD.

% TODO: write about the significance of particular segments?

SocketCAN is fairly flexible and offers either highly customized configuration
by setting all the segment durations manually, or a convenient configuration by
setting just the bitrate and sample point (and even that is chosen automatically
per Bosch recommendation if not specified). However, each CAN controller may
have different base clock frequency and different width of segment duration
registers. The algorithm thus needs the minimum and maximum values for the
durations (and clock prescaler) and tries to optimize the numbers to fit both
the constraints and the requested parameters.

\begin{listing}
    \begin{minted}[autogobble]{c}
        struct can_bittiming_const {
            char name[16];      /* Name of the CAN controller hardware */
            __u32 tseg1_min;    /* Time segement 1 = prop_seg + phase_seg1 */
            __u32 tseg1_max;
            __u32 tseg2_min;    /* Time segement 2 = phase_seg2 */
            __u32 tseg2_max;
            __u32 sjw_max;      /* Synchronisation jump width */
            __u32 brp_min;      /* Bit-rate prescaler */
            __u32 brp_max;
            __u32 brp_inc;
        };
    \end{minted}
    \cprotect\caption{Fields of \verb|can_bittiming_const|}
    \label{lst:can_bittiming_const}
\end{listing}

A curious reader will notice that the durations of the segments
\textsc{PROP\_SEG} and \textsc{PHASE\_SEG1} are not determined separately but
rather combined and then, by default, the resulting TSEG1 is evenly divided
between \textsc{PROP\_SEG} and \textsc{PHASE\_SEG1}. In practice, this has
virtually no consequences as the sample point is between \textsc{PHASE\_SEG1}
and \textsc{PHASE\_SEG2}. In CTU CAN FD, however, the duration registers
\verb|PROP| and \verb|PH1| have different widths (6 and 7 bits, respectively),
so the auto-computed values might overflow the shorter register and must thus be
redistributed among the two\footnote{As is done in the low-level driver
functions \verb|ctu_can_fd_set_nom_bittiming| and
\verb|ctu_can_fd_set_data_bittiming|.}.



\subsection{Handling RX}

Frame reception is handled in NAPI queue, which is enabled from ISR when the
RXNE (RX FIFO Not Empty) bit is set. Frames are read one by one until either no
frame is left in the RX FIFO or the maximum work quota has been reached for the
NAPI poll run (see \fref{sec:socketcan:napi}). Each frame is then passed to the
network interface RX queue.

An incoming frame may be either a CAN 2.0 frame or a CAN FD frame. The way to
distinguish between these two in the kernel is to allocate either
\verb|struct can_frame| or \verb|struct canfd_frame|, the two having different
sizes. In the controller, the information about the frame type is stored in the
first word of RX FIFO.

This brings us a chicken-egg problem: we want to allocate the \verb|skb| for the
frame, and only if it succeeds, fetch the frame from FIFO; otherwise keep it
there for later. But to be able to allocate the correct \verb|skb|, we have to
fetch the first work of FIFO. There are several possible solutions:
\begin{enumerate}
    \item Read the word, then allocate. If it fails, discard the rest of the
          frame. When the system is low on memory, the situation is bad anyway.
    \item Always allocate \verb|skb| big enough for an FD frame beforehand. Then
          tweak the \verb|skb| internals to look like it has been allocated for
          the smaller CAN 2.0 frame.
    \item Add option to peek into the FIFO instead of consuming the word.
    \item If the allocation fails, store the read word into driver's data. On
          the next try, use the stored word instead of reading it again.
\end{enumerate}

Option 1 is simple enough, but not very satisfying if we could do better. Option
2 is not acceptable, as it would require modifying the private state of an
integral kernel structure. The slightly higher memory consumption is just a
virtual cherry on top of the ``cake''. Option 3 requires non-trivial HW changes
and is not ideal from the HW point of view.

Option 4 seems like a good compromise, with its disadvantage being that a
partial frame may stay in the FIFO for a prolonged time. Nonetheless, there may
be just one owner of the RX FIFO, and thus no one else should see the partial
frame (disregarding some exotic debugging scenarios). Basides, the driver
resets the core on its initialization, so the partial frame cannot be
``adopted'' either.
In the end, option 4 was selected\footnote{At the time of writing this thesis,
option 1 is still being used and the modification is queued in gitlab issue
\#222}.

% TODO: aside - alloc failure handling in the rest of the drivers - include it?

\subsubsection{Timestamping RX frames}
\label{subsec:ctucanfd:rxtimestamp}
The CTU CAN FD core reports the exact timestamp when the frame has been
received. The timestamp is by default captured at the sample point of the last
bit of EOF but is configurable to be captured at the SOF bit. The timestamp
source is external to the core and may be up to 64 bits wide. At the time of
writing, passing the timestamp from kernel to userspace is not yet implemented,
but is planned in the future.


\subsection{Handling TX}

The CTU CAN FD core has 4 independent TX buffers, each with its own state and
priority. When the core wants to transmit, a TX buffer in Ready state with the
highest priority is selected.

The priorities are 3bit numbers in register TX\_PRIORITY (nibble-aligned).
This should be flexible enough for most use cases. SocketCAN, however, supports
only one FIFO queue for outgoing frames\footnote{Strictly speaking, multiple CAN
TX queues are supported since v4.19 \cite{linux:patch-canmq} but no mainline
driver is using them yet.}. The buffer priorities may be used to simulate the
FIFO behavior by assigning each buffer a distinct priority and \textit{rotating}
the priorities after a frame transmission is completed.

In addition to priority rotation, the SW must maintain head and tail pointers
into the FIFO formed by the TX buffers to be able to determine which buffer
should be used for next frame (\verb|txb_head|) and which should be the first
completed one (\verb|txb_tail|). The actual buffer indices are (obviously)
modulo 4 (number of TX buffers), but the pointers must be at least one bit wider
to be able to distinguish between FIFO full and FIFO empty -- in this situation,
$txb\_head \equiv txb\_tail\ (\textrm{mod}\ 4)$. An example of how the FIFO is
maintained, together with priority rotation, is depicted in
\fref{fig:ctucanfd:txpriorot}.

\begin{figure}[tb]
    \centering
    \begin{subfigure}[t]{0.32\textwidth}
    \begin{tabular}{lc|c|c|c}
    TXB\# & 0 & 1 & 2 & 3 \\\hline
    Seq   & A & B & C &   \\
    Prio  & 7 & 6 & 5 & 4 \\\hline
          &   & T &   & H \\
    \end{tabular}
    \caption{3 frames are queued.}
    \end{subfigure}
    ~%
    \noindent
    \begin{subfigure}[t]{0.32\textwidth}
    \begin{tabular}{lc|c|c|c}
    TXB\# & 0 & 1 & 2 & 3 \\\hline
    Seq   &   & B & C &   \\
    Prio  & 4 & 7 & 6 & 5 \\\hline
          &   & T &   & H \\
    \end{tabular}
    \caption{Frame A was successfully sent and the priorities were rotated.}
    \end{subfigure}
    ~%
    \noindent
    \begin{subfigure}[t]{0.32\textwidth}
    \begin{tabular}{lc|c|c|c|c}
    TXB\# & 0 & 1 & 2 & 3 & 0' \\\hline
    Seq   & E & B & C & D &    \\
    Prio  & 4 & 7 & 6 & 5 &    \\\hline
          &   & T &   &   & H  \\
    \end{tabular}
    \cprotect\caption{2 new frames (D, E) were enqueued. Notice that the
        priorities did not have to be adjusted. \verb|txb_head| now contains the
        value 5 which indicates TXB\#0, but allows us to detect that all buffers
        are full.}
    \end{subfigure}
    \cprotect\caption[CTU CAN FD TXB priority rotation example]{TXB priority
        rotation example. Empty Seq means the buffer is empty. Higher priority
        number means higher priority. H and T mark \verb|txb_head| and
        \verb|txb_tail|, respectively \cite{doc:ctucanfd}.}
        \label{fig:ctucanfd:txpriorot}
\end{figure}

\begin{figure}[bt]
    \centering
    %\includegraphics[width=\linewidth]{figures/txbuf_states}
    \includegraphics[width=\linewidth]{figures/ctucanfd_FSM_TXT_Buffer}
    \caption[TX Buffer states with possible transitions]
        {TX Buffer states with possible transitions \cite{doc:ctucanfd}.}
    \label{fig:ctucanfd:txbuf_states}
\end{figure}

\subsubsection{Timestamping TX frames}
\label{subsec:ctucanfd:txtimestamp}

When submitting a frame to a TX buffer, one may specify the timestamp at which
the frame should be transmitted. The frame transmission may start later, but not
sooner. Note that the timestamp does not participate in buffer prioritization --
that is decided solely by the mechanism described above.

Support for time-based packet transmission was recently merged to Linux v4.19
\cite{lwn:ttnet}, but it remains yet to be researched whether this functionality
will be practical for CAN.

Also similarly to retrieving the timestamp of RX frames, the core supports
retrieving the timestamp of TX frames -- that is the time when the frame was
successfully delivered. The particulars are very similar to timestamping RX
frames and are described in \fref{subsec:ctucanfd:rxtimestamp}.

\subsection{Handling RX buffer overrun}

When a received frame does no more fit into the hardware RX FIFO in its
entirety, RX FIFO overrun flag (STATUS[DOR]) is set and Data Overrun Interrupt
(DOI) is triggered. When servicing the interrupt, care must be taken first to
clear the DOR flag (via COMMAND[CDO]) and after that clear the DOI interrupt
flag. Otherwise, the interrupt would be immediately\footnote{Or rather in the
next clock cycle} rearmed.

\textbf{Note}: During development, it was discussed whether the internal HW
pipelining cannot disrupt this clear sequence and whether an additional dummy
cycle is necessary between clearing the flag and the interrupt. On the Avalon
interface, it indeed proved to be the case, but APB being safe because it uses
2-cycle transactions. Essentially, the DOR flag would be cleared, but DOI
register's Preset input would still be high the cycle when the DOI clear request
would also be applied (by setting the register's Reset input high). As Set had
higher priority than Reset, the DOI flag would not be reset. This has been
already fixed by swapping the Set/Reset priority (see issue \#187).

\subsection{Reporting Error Passive and Bus Off conditions}

It may be desirable to report when the node reaches \textit{Error Passive},
\textit{Error Warning}, and \textit{Bus Off} conditions. The driver is notified
about error state change by an interrupt (EPI, EWLI), and then proceeds to
determine the core's error state by reading its error counters.

There is, however, a slight race condition here -- there is a delay between the
time when the state transition occurs (and the interrupt is triggered) and when
the error counters are read. When EPI is received, the node may be either
\textit{Error Passive} or \textit{Bus Off}. If the node goes \textit{Bus Off},
it obviously remains in the state until it is reset. Otherwise, the node is
\textit{or was} \textit{Error Passive}. However, it may happen that the read
state is \textit{Error Warning} or even \textit{Error Active}. It may be unclear
whether and what exactly to report in that case, but I personally entertain the
idea that the past error condition should still be reported. Similarly, when
EWLI is received but the state is later detected to be \textit{Error Passive},
\textit{Error Passive} should be reported.

% issue #185

\endinput
b) Implement and test Linux SocketCAN driver for CTU CAN FD soft IP core controller.
- CTU CAN FD
    - overview: what it is, when and where it originated, resurrection of the project - optimizations, SocketCAN driver
    - SW part
        - SocketCAN API overview
            - device probe
                - info loaded from device tree
                - what is done on probe, what on open; bitrate set, other things ...
                - bit timing computation
            - device setup
            - TX: packet from TX queue - copy from doc, make images
            - ISR:
                - errors
                - TX done/error
                - RXBNE -> wake up NAPI queue
            - RX
                - aside: NAPI
                - fetch and enqueue the frames
                - copy from doc, make images
        - The particulars for CTU CAN FD
            - overview: low-level driver + kernel driver, auto generation
                - design choices: bitfields, pros and cons
            + Hitch: setting bit timing
                - reg width, rebalancing... copy from doc
            - Hitch: data overrun interrupt
                - clearing the flag
                - pipelining considerations
            - Interesting case: EWL/EP/BUSOFF reporting and race conditions
                - why report and what to report? see issue #185
            - TX done handling:
                - fixing all the race conditions - copy from doc & code
                - FIFO, priority rotation, commands
            + RX: distinguishing between CAN 2.0 and CAN FD frame
                - flag in first FIFO word (status word)
                - chicken and egg:
                    - allocate SKB for the frame and only if it succeeds, fetch the frame from FIFO; otherwise keep it there for later
                    - to allocate the SKB, we have to know it it's FDF or not -> we have to fetch first word from FIFO
                - Solution #1:
                    - If we're low on memory, we're screwed anyway -> just ditch the frame
                - Solution #2:
                    - always allocate FD SKB
                    - hitch: we'd have to modify the SKB internals to reset its length - that's ugly
                - Solution #3:
                    - add option to peek into the FIFO instead of consuming the word
                    - best solution, will employ when the HW register is added
                - Solution #4 - remember the word for next time
                - another interesting case: what happens on SKB alloc fail?
                    - analysis of other drivers
                        - some will correctly breal
                        - some will busy-loop in NAPI task
                        - some ..??? TODO: research
                    - hard to tell what is correct, as said above - when we're that low on memory, things get weird anyway
