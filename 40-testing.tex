\chapter{Testing}
\label{ch:testing}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Simulation framework}

The core already included a set of simulation testbenches together with a set of
simple TCL scripts for Modelsim to run them. Together with saved waveform
layouts, this was perfect for debugging or semi-automated testing, but as more
and more changes were performed in the core, it became apparent that a fully
automated testing workflow would benefit us immensely.

Although Modelsim's debugging capabilities are indisputable superior, it was
decided to use an open-source simulator -- GHDL \cite{web:ghdl}, for the
automated testing. The reasons behind this decision include:
\begin{itemize}
\item GHDL's simulation speed is better than that of free Modelsim edition
\item Possible license issues with Modelsim usage
\item Support for line coverage
\item Support for functional coverage via PSL
\end{itemize}


Since the core repository is hosted on the university Gitlab
\cite{git:ctucanfd}, it is only natural to leverage Gitlab's Continuous
Integration.


\subsection{CTU CAN FD Testcases overview}

The core includes an extensive set of verification tests. The tests may be
divided into several groups, based mainly on the test interface:
\begin{itemize}
    \item Unit tests
    \item Feature tests
    \item Sanity test
    \item Reference test
\end{itemize}

The unit tests generally verify the individual small entities, whereas the
feature tests check the behavior of the core as a whole in various scenarios.
The sanity test instantiates multiple instances of the core, connected in a
specified topology with simulated transceiver and bus delay, and has them
communicate with each other. The reference test replays the bus traffic logged
from a reference CAN FD controller and checks that the core receives all the
frames correctly. The tests are in detail described in the core documentation
\cite{doc:ctucanfd} in chapter 5.



\subsection{Installing GHDL}
\label{subsec:test:ghdl}

GHDL compiles the simulation sources to a machine executable binary. The code
generation may be performed by 3 different backends: GCC, LLVM or mcode. While
the gcc-flavoured ghdl is the most complicated to build, it is the only one that
is able to provide a basic code coverage functionality. Although the code
coverage works almost ``by accident'', for basic line coverage, this is fairly
accurate. While it is undoubtedly true that professional IPs get tested while
observing expression coverage, toggle coverage and many more, the free tools in
the area of digital design do not possess these advanced capabilities.
% TODO: je to nejaky blbe formulovany

Gcc-flavoured GHDL has requirements on the version of gcc which is building it
and on the version used to compile the testbenches. Moreover, if it is desired
to use the code coverage feature, the version of the GCOV library used during
compilation and when running the tests must be the same. The manual process of
setting up all the dependencies is rather complicated and fragile. Fortunately,
this complexity and inter-dependency are eliminated by containerizing it all in
Docker.

Docker is a tool for lightweight ``virtualization'', implemented via operating
system containers. Most instruction files to build a typical docker image
share common patterns: based on a specific distribution image (e.g., debian
stable), install some packages, build an application, copy over some
user-supplied files and package it all in a ready-to-use image. This image is
then later used as a base to run a \textit{container} executing the desired
command.

Moreover, GHDL has set up their own CI/CD and the most recent docker images,
with prebuilt ghdl, are published on Docker Hub. Using Docker is thus very
convenient in this instance. Docker images are also the most common way to use
custom build environment in Gitlab CI.

\subsection{Extending the simulation framework}

In addition to the testbenches and simulator, it is necessary to:
\begin{itemize}
\item build the sources
\item run the testbenches, with configurable parameters
\item collect test results and show a summary
\end{itemize}

This is a non-trivial amount of work to do; however, the requirements are not
very special. A suitable simulation framework was searched for to use as a
basis, and eventually it was decided to use VUnit.

\subsubsection{VUnit}

VUnit is a complex HDL simulation framework. It is written in Python, with the
HDL support libraries written in VHDL. It also supports a lot of simulators,
including GHDL and Modelsim. So as a bonus, it is still possible to open the
simulation in Modelsim and use the prepared weveform layouts to diagnose
problems quickly, with modelsim-specific code kept to the minimum.

It implements both the running part (compile, run, collect results) and the VHDL
part (error reporting, logging library, setting up test timeout, and many more).



\subsubsection{The framework}

The test framework is located in the CTU CAN FD repository in
\path{/test/testfw}, with its entry point \path{/test/run.py}. It is a command
line Python application, whose primary purpose is to run the desired tests. All
the compiling and running functionality comes directly from VUnit. What the
framework adds is mainly unified configuration and the ability to set up
waveforms in GUI mode automatically.

The framework supports several types of tests, grouped by common interface: unit
tests, feature tests, sanity test, and reference test.

\begin{listing}[ht]
    \begin{minted}[autogobble]{yaml}
        unit:
            default:
                log_level: warning
                iterations: 50
                timeout: 100 ms
                error_tolerance: 0
                # randomize: false
                # seed: 0 # use to reconstruct results from randomized runs
            tests:
                bit_stuffing:
                    iterations: 10
                    wave: unit/Bit_Stuffing/bsdt_unit.tcl
                apb:
                    iterations: 1
                bus_sync:
                    wave: unit/Bus_Sampling/bsnc_unit.tcl
        feature:
            default: {...}
            tests:
                abort_transmittion:
                arbitration:
                fault_confinement:
    \end{minted}
    \caption{Example of test suite configuration}
    \label{lst:testfw:config}
\end{listing}

The configuration is hierarchical, with values from the default section being
inherited by the particular tests if not overridden. That enables both
fine-grained configurability and concise test definition if no special treatment
is needed.

As there is a lot of unit tests and feature tests, it is quite possible that
when adding a new one, the implementor forgets to add it to the configuration.
Fortunately, the framework detects such unconfigured tests and prints a clearly
visible warning.

Most of the configuration options are passed to the VHDL test case as generics
of the top-level entity. GHDL does not support passing composite types (arrays,
records, vectors) in this way, some values have to be serialized on the Python
side and again deserialized on VHDL side.

More information about the particular tests may be found in CTU CAN FD
documentation \cite{doc:ctucanfd}.

\subsubsection{Running tests}
\label{subsec:test:running}

Running the tests requires Python 3 and dependencies specified in
\path{/test/testfw/requirements.txt}, as well as at least one HDL simulator. All
simulators supported by VUnit are supported, but it has been only tested (and
optimized for) Modelsim and GHDL.

In case no simulator is set up on a computer, one may use the prepared Docker
image with all the dependencies and GHDL. A convenience script to run the tests
in the docker image is included as \path{/run-docker-test} in the CTU CAN FD
repository.

\begin{listing}[ht]
    \begin{minted}[autogobble]{bash}
        # Print help for the test subcommand
        $ ./run.py test --help

        # Run all tests from tests_fast.yml
        $ ./run.py test tests_fast.yml

        # Print help for VUnit; you may specify VUnit options after --
        $ ./run.py test tests_fast.yml -- --help

        # List all tests
        $ ./run.py test tests_fast.yml -- --list

        # Read configuration from tests_fast.yml, but run just one test
        $ ./run.py test tests_fast.yml lib.tb_feature.retr_limit

        # Run all feature tests (configured in tests_fast.yml)
        $ ./run.py test tests_fast.yml 'lib.tb_feature.*'

        # Force using a specific simluator. Read more in VUnit documentation.
        $ VUNIT_SIMULATOR=modelsim ./run.py test tests_fast.yml

        # Run a test in GUI mode
        $ ./run.py test tests_fast.yml lib.tb_feature.retr_limit -- -g
    \end{minted}
    \caption{Examples of running simulation tests}
    \label{lst:testfw:run}
\end{listing}

\subsubsection{Automatic waveform layout in GUI mode}

When debugging with a test case, one generally wants to observe a given set of
internal signals and preserve this view between runs. Each test case has its own
specific set of useful signals, and this layout may be saved in a file and
attached to the test case in configuration, as may be seen in
\fref{lst:testfw:config}.

At the time of writing, the layout files are only supported when simulating in
Modelsim, because they originated as Modelsim-specific TCL files. There is,
however, a development version\footnote{That might be found in branch
\verb|gtkw-gui| in the CTU CAN FD repository}, which may either specify native
\verb|gtkwave| layout files, or attempt to create them from the Modelsim TCL
file. To be reasonably usable, it requires patching VUnit and still has some
minor problems. Therefore, it has not been merged yet.

\subsubsection{Test results}

The test results are printed to the terminal, with a summary at the end,
but also gathered in a JUnit-compatible XML file, which may then be rendered in
an HTML browser using a shipped XSLT stylesheet.

\begin{figure}[bt]
    \centering
    \includegraphics[width=\linewidth]{figures/testfw_res}
    \caption[Example test results from the testing framework.]
            {Example test results from the testing framework. After clicking on
            a test case name, its output is shown.}
    \label{fig:testfw:res}
\end{figure}

\subsubsection{Line coverage}

As mentioned above in \fref{subsec:test:ghdl}, GHDL with GCC backend supports
generating line and function\footnote{Not to be confused with functional
coverage, which is also supported, but via different means.} coverage using
GCOV. This is achieved by specifying GHDL flags \verb|-fprofile-arcs|
\verb|-ftest-coverage| during the analyze phase and \verb|-Wl,-lgcov|
\verb|-Wl,--coverage| \verb|-Wl,-no-pie| during the elaboration phase.

During the compilation, one part of profile data is generated by the compiler
(the \verb|.gcno| files) and when the tests are run, the second part of the
profile data is generated in the form of \verb|.gcda| files. All the profile
data are then processed by commands \verb|lcov| and \verb|genhtml|, as seen in
\fref{lst:test:lcov}.

The necessary compile options for GHDL are enabled in the testing framework in
\path{/test/testfw/test_common.py} and the generating of HTML coverage report is
handled in the supplied Makefile\footnote{\path{/test/Makefile}}. A link to the
report for the most recent core version is available on the project's gitlab
page \cite{git:ctucanfd}.

\begin{listing}
    \begin{minted}[autogobble]{bash}
        $ lcov --capture --directory build --output-file code_coverage.info
        $ genhtml code_coverage.info --output-directory code_html
    \end{minted}
    \caption{Generating HTML report from test code coverage}
    \label{lst:test:lcov}
\end{listing}





% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Gitlab CI}

\begin{comment}
- build, test, deploy pipelines
- different triggers - push, schedule, ..
- email on failure


- Sources at https://gitlab.fel.cvut.cz/jerabma7/gitlab-webhook-trigger
    - nginx, app, docker registry
- gitlab runner
- updated using cron job (docker pull, rebuild, restart containers)
- debian server with unattended upgrades, mail notifiations using msmtp, recursive aliases
- problem with building documentation, vagrant and Arch box with gitlab-runner with local executor

- extract minimum Linux sources necessary for building out-of-tree modules
-
\end{comment}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Automated builds}
\label{sec:tst:autobuild}

For automated testing on actual hardware, it is desirable to be able to build
the FPGA bitstream automatically from the freshest sources. In addition, as the
synthesis takes a rather long time (around 20 minutes), it would be wasteful to
rebuild the image after every change. Before delving into the particulars of the
system setup, you might want to revise the project structure and repositories in
\ref{sec:intro:repos}. The following requirements were gathered:
\begin{itemize}
\item Before the build, submodules should be automatically updated to the
      newest version.
\item The build should run only if the sources changed (including submodules).
\item The build should run only once a day, ideally at night when the build
      server is free.
\end{itemize}

This is a tricky combination for Gitlab's CI, as it may trigger the pipeline on
push (changes) or periodically (at night), but not on their combination. Another
difficulty comes with updating the submodules -- but that actually presents a
nice solution.

Every night:
\begin{itemize}
    \item The master branch is merged into an autobuild branch.
    \item Submodules are updated in the autobuild branch.
    \item The autobuild branch is pushed to the repository.
\end{itemize}

Finally, on a push to any autobuild branch, the build job (and subsequent tests)
is triggered. The build may also be triggered manually by creating a pipeline
for the autobuild branch\footnote{Or the master branch, in which case the
autoupdates and pushes are performed.}. For the future, there are planned
multiple autobuild branches, each for different stability phase of the
submodules (i.e., stable, bleeding edge, etc.). The detailed CI configuration
may be found in the top-level repository in \path{/.gitlab-ci.yml}.

\subsection{Pushing to repository from CI job}

Unfortunately, gitlab does not yet provide read-write deploy tokens for CI jobs,
and thus an alternative approach must be used. A dedicated SSH key pair is
generated for the CI. The public key is registered into Gitlab (under some user)
and the private key is made available to the job runner.

The private key may be passed to the runner via Gitlab CI secret variables, but
since we have our own runner anyway, it is possible to keep the private key only
on the build server, in a docker volume, and specify that the volume should be
mounted in the project's runner.

\begin{listing}
    \begin{minted}[autogobble]{toml}
        [[runners]]
          ...
          executor = "docker"
          [runners.docker]
            volumes = ["/cache", "/opt/xilinx:/opt/xilinx:ro",
                       "depkey_cantop:/depkey:ro"]
    \end{minted}
    \cprotect\caption{Specifying a volume in \path{config.toml} for
                      gitlab-runner}
    \label{fig:gitlab:runnercfg}
\end{listing}

\subsection{Making Vivado available in the build image}

A nice thing about docker images is that they are self-contained and may run
everywhere. It is thus convenient to upload the built image to Docker Hub
and let whichever runner download it. That is not possible with the synthesis
tools, as they are not free and must not be distributed.

One solution is to create a \textit{private} image on Docker Hub or use our own
Docker Registry\footnote{Self-hosted backend of Docker Hub}. Unfortunately, the
synthesis tools also take a lot of disk space.

Taking inspiration from the solution with deploy keys (see the previous
section), the Vivado synthesis tools are made available to the runner as a bind
mount. That way, we are limited on our one runner, but we do not have to be
concerned about gigabytes of licensed software traveling around the network.
The configuration may be seen in \fref{fig:gitlab:runnercfg}.


\section{Automated FPGA tests}
\label{sec:tst:hwtest}

While most hardware problems should be caught in simulation, it is still
desirable to perform at least some simple tests on real hardware, for several
reasons:
\begin{itemize}
    \item There might be problems introduced by synthesis.
    \item Simulation is slow and can only perform that many tests.
    \item It is an opportunity also to test the driver.
    \item It is an opportunity also to test interoperability with different
          controllers.
\end{itemize}

After every automated build (described in \fref{sec:tst:autobuild}):
\begin{enumerate}
    \item The FPGA bitstream, CTU CAN FD driver, and the whole test suite is
          uploaded to the MicroZed testing board.
    \item The new bitstream is loaded into the FPGA (see
          \fref{subsec:tst:fpgaflash}).
    \item The new kernel driver is loaded.
    \item The test suite is run (see \fref{subsec:tst:testsuite}).
    \item The results are collected and downloaded to the job runner.
\end{enumerate}

\subsection{Updating FPGA bitstream}
\label{subsec:tst:fpgaflash}

The FPGA bitstream may be updated in 3 distinct ways:
\begin{enumerate}
\item via U-Boot on boot (requires restart)
\item via \path{/dev/xdevcfg} (requires Xilinx-flavored kernel)
\item via kernel FPGA Manager interface
\end{enumerate}

% TODO: 1, 2
For the purposes of automated testing on HW, variant 3 was chosen for this
project as it is the most elegant one. The FPGA Manager subsystem finally got
into the mainline kernel in v4.4 with significant upgrades later on. Unlike the
previous vendor-specific implementations, the FPGA Manager approach addresses
the fact that when the bitstream is updated, the hardware \textit{changes}. Old
devices disappear, and new ones appear. That means the old devices must be
deinitialized before the update and new devices must be probed afterward. Even
if the uploaded bitstream is identical to its predecessor, the hardware is
reset, and the drivers needs to reinitialize it.

The question is how to tell the kernel \textit{which} devices will appear or
disappear. Traditionally, there is one monolithic Device Tree Blob (DTB) which
is loaded at boot time, and it contains definitions of both Hard Core
peripherals and Soft Core peripherals implemented in FPGA.
Alternatively, the Device Tree may be divided into a base image and one or more
Device Tree Overlays. This elegantly solves the problem, as the FPGA image is
paired with a DTO, describing the peripherals implemented by the bitstream. When
a bitstream is loaded, the DTO is applied afterward and the kernel probes the
newly appeared device. Similarly, when the bitstream is about to be removed (or
replaced), the DTO is unloaded and the disappearing devices are correctly
deinitialized (together with all bookkeeping data structures in the kernel).

\subsubsection{Using FPGA Manager from userspace}

The FPGA Manager interface is directly available only from the kernel itself.
For some reason, the mainline kernel does not provide any method to access this
interface from user space. Fortunately, there exists an external module
\verb|dtbocfg|\footnote{Available at \url{https://github.com/ikwzm/dtbocfg}}. It
creates a directory hierarchy under ConfigFS, which may be accessed using
standard shell commands.

Full explanation and description of how to pack the bitstream, configure the
kernel, set up the device tree, and finally apply the overlay are given in
\cite{xilinx:dtbo,dtbocfg}. A very brief summary is also given in listings
\ref{lst:test:fpgam:bitstream}, \ref{lst:test:fpgam:load}, and
\ref{lst:test:fpgam:dto}.

\begin{listing}[bht]
    \begin{minted}[autogobble]{bash}
        # Wrap the BIT bitstream as BIN
        # system.bif contains just "all: {system.bit}"
        bootgen -image system.bif -w -process_bitstream bin

        # Copy the bitstream into /lib/firmware
        cp system.bit.bin /lib/firmware/system.bit.bin

        # Compile the base Device Tree
        dtc -O dtb -b 0 -@ -o base.dtb base.dts

        # Compile the Device Tree Overlay
        dtc -O dtb -b 0 -@ -o overlay.dtbo overlay.dts
    \end{minted}
    \caption{Preparing the FPGA bitstream for loading via FPGA Manager
             interface and compiling the Device Trees.}
    \label{lst:test:fpgam:bitstream}
\end{listing}

\begin{listing}[bht]
    \begin{minted}[autogobble]{bash}
        # Load the module
        modprobe dtbocfg

        # Create new overlay under ConfigFS. The name is not important.
        cd /sys/kernel/config/device-tree/overlays
        mkdir fpga-overlay
        cd fpga-overlay

        # Load the DTBO file contents
        cat overlay.dtbo >dtbo

        # Activate the DT overlay
        echo 1 >status
    \end{minted}
    \caption{Loading the FPGA bitstream via FPGA Manager interface.}
    \label{lst:test:fpgam:load}
\end{listing}

\begin{listing}[bht]
    \begin{minted}[autogobble]{dts}
        /dts-v1/;
        /plugin/;
        / {
            fragment@0 {
                target-path = "/fpga-full";

                __overlay__ {
                    #address-cells = <1>;
                    #size-cells = <1>;

                    firmware-name = "system.bit.bin";
                };
            };

            fragment@1 {
                target-path = "/amba";
                __overlay__ {
                    /* new devices */
                };
            };
        };
    \end{minted}
    \caption{Example of device tree overlay source.}
    \label{lst:test:fpgam:dto}
\end{listing}


\subsection{The test suite}
\label{subsec:tst:testsuite}

The tests are written in Python using the Pytest framework and pycan for CAN bus
communication. The sources are located in the top-level repository in
\path{/ci/cantest}. This is an ongoing effort as more tests may always be added.
The progress may be observed in the top-level project's issue \#1.

Tests:
\begin{itemize}
    \item Check that all the required CAN interfaces are present (sanity test).
    \item Run CTU CAN FD's \textit{regtest} tool, which checks the IP
          integration (word, halfword, and byte reads and writes work correctly,
          identification register contains expected value).
    \item Series of communication tests, with one transmitter and one or more
          receivers.
\end{itemize}

Unstable tests in development:
\begin{itemize}
    \item RX FIFO Overrun test.
    \item Series of communication tests with multiple transmitters.
\end{itemize}

For the communication tests, both CTU CAN FD and SJA1000 FD-tolerant controllers
are tested. The tests have variants based on which of the controllers is the
transmitter (CTU CAN FD or SJA1000) and on the communication mode (only CAN 2.0
frames, ISO FD frames, non-ISO FD frames).

To reliably test the handling of RX FIFO Overrun, the particular test requires a
special debug \verb|ioctl| in the device driver to disable or re-enable handling
of RX frames in the driver.

For the communication tests with multiple transmitters, care must be taken when
randomly generating the CAN IDs. If multiple nodes try to transmit a frame with
identical CAN ID (but different payload) at the same time, the mismatch in
\textsc{Data Phase} will be interpreted as a \textsc{Bit-Error} instead of
\textsc{Arbitration-Loss}. This fact was overlooked at first, and the core was
thought to contain a bug -- which is not the case in this instance; the behavior
is consistent with the CAN specification.

During the tests, the kernel message log (\verb|dmesg|) is monitored, and if a
message with severity Warning or above appears during a test, the test fails.
Also, the network interface error counters are checked and must be zero;
otherwise the test fails.

\endinput
c) Implement and document automated testing via continuous integration for CTU CAN FD.
    - Testing
        - background of tests: unit & feature tests, sanity test
        - a lot of changes, optimizations done on the HW
        - a need for automated testing
        - there was a simple testing framework, but mainly for manual tests and debugging; in modelsim, which is not free and slow for big designs
        - Simulation Test FW
            - Decision 1: Use GHDL as the primary simulator
                - advantage: it's fast (compared to free modelsim version)
                - advantage: it costs nothing
                - advantage: it supports rudimentary line coverage
            - Decision 2: we still want to be able to use Modelsim for debugging
                - superior waveform viewer + we already have layout configurations for all the tests
            - Decision 3: Use VUnit as the test framework
                - multiplatform, written in Python, flexible
                - supports multiple simulators (GHDL and Modelsim are interesting for us)
                - also offers runtime logging and checking libraries (which we do not use yet)
            - TODO: FW design
                + config files
                + layout files
                + idea: generate layout files for gtkwave via TODO: python lib
                    - pyvcd, Tkinter
            - VHDL coverage: howto, hitches (gcov version mismatch)
            - GHDL compiling pitfails, using prebuilt docker image
        - Gitlab CI
            - quick tests
            - nightly run
            - triggering on merge instead of on push
        - Automated testing on HW
            + automated nightly fetch & build of FPGA bitstream
                + gitlab runner on custom server with Xilinx Vivado image
                  (cannot push to docker hub because of license)
            - upload test scripts, bitstream and driver via SSH to testing board
            - update bitstream, kernel, run tests
            - Updating bitstream
                - Variant A: via U-Boot (requires reboot)
                - Variant B: Xilinx's proprietary xdevcfg - but that's not present in mainline kernel
                - Variant C: use standard FPGA manager subsystem
                    - ConfigFS
                    - Device tree overlay
                    - how is it done
            - Tests
                + in python, using pytest, pycan
                + what tests do we have
                    - ...
                - issue: unable to trigger high loads from python -> Python module
                - RX overflow test: need for debug ioctl
                - discovered problem: bidir traffic - but the Core contains known bugs, which will be addressed
- hathi docker stack
