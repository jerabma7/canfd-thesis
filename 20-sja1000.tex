\chapter{Making OpenCores SJA1000 FD-Tolerant}
\label{ch:sja1000}

In \fref{sec:intro:sja}, it was stated that an FD-tolerant controller was needed
until the CTU CAN FD controller was ready and that SJA1000 from OpenCores was
selected as a basis for this purpose. The following chapter describes the
modifications done to the core in order to make it FD-tolerant, together with
some interesting problems encountered on the way.

Solving the problems brought me further understanding of the CAN standard and
made me understand the reasoning behind some particulars of the protocol. Parts
of the standard looking as some random details turned out to be a vital part of
the specification in many corner cases (which may still happen rather
frequently).

The changes to the core may be summed up roughly to these points. The following
sections will discuss the details of them.
\begin{itemize}
    \item Detection of the borders of an FD frame (especially of its end) and
          properly ignoring it.
    \item Circumventing the FSM during FD ignore period (adding a condition to
          all transitions).
    \item Preventing TX during FD ignore period.
    \item Fix waiting for bus idle after bus-off to enable going out of reset.
\end{itemize}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Basic idea}
\label{sec:sja:idea}

The idea of making the controller FD-tolerant is fairly simple: If the EDL bit,
indicating an FD frame, is set, ignore the rest of the frame and resume normal
bus participation only after the FD frame is finished.

We thus have to detect two things:
\begin{itemize}
\item Beginning of the ignore period
\item End of the ignore period
\end{itemize}

Finding the beginning is trivial -- the received EDL bit is recessive,
indicating an FD frame. The end condition is a bit more complicated. To know the
length of the frame, it would be necessary to understand the CAN FD protocol
and to know the data bitrate -- but that way we would end up implementing the
FD support, which is not trivial at all.

Fortunately, the CAN standard is designed in a way that it \textit{is} possible
to detect when the bus is idle:
\begin{itemize}
\item Every frame must be followed by Interframe space -- at least 3 recessive
      bits
\item Every data frame ends with an EOF field consisting of 7 recessive bits (+
      8th for ACK delimiter)
\item Every error (or overload) frame ends with Error delimiter field
      consisting of 8 recessive bits.
\end{itemize}

Remote frames do not have to be considered since remote frames do not exist in
FD format. They are, however, structurally identical to data frames. A curious
reader may wonder why the amount of bits in EOF and Error Delimiter differs --
surely it would seem more sensible to have them the same length, so that the
minimum ``space'' between frames is the same, regardless of the frame type.
However, upon further inspection of the data frame, one may note that there is 1
extra recessive bit before the EOF field -- ACK Delimiter. This small detail
remained unrecognized at first and had led to several difficulties during the
testing, which were resolved after repeated examination of the CAN protocol
specification.

% TODO: link to further section

% TODO: frame images with highlighted recessive space

\begin{comment}
Error frame (EA node)
EFlag |EDelim  |ifs|SOF + next frame
000000|11111111|111|0dddd

Data frame
crc delim| ack slot | ack delim | EOF     | ifs | SOF + data
1        | 0/1      | 1         | 1111111 | 111 | 0dddd
\end{comment}

So for the end condition, it is enough to wait for 8 consecutive \recessive{}
bits and then go to \textsc{Intermission}. This condition may not occur in the
middle of frame because of bit stuffing. The alternative of waiting for 11
consecutive \recessive{} bits instead and then go to \textsc{Idle} is not
acceptable -- there would arise obscure problems with Early TX,
resynchronization and suspend, which are already being handled correctly in the
\textsc{Intermission} field. See \fref{sec:sja:hardsync} for details.

This again looks simple enough, but in practice, several problems arise.

\section{The bitrate shift}

In FD frames, the data part may be transmitted in a different (higher) bitrate,
but the SJA1000 controller does not process this part of the frame nor does it
detect the bitrate shift. If we sample the bus only in sample point of the
nominal bitrate, we miss some bits. That might deceive the detection of the FD
ignore period.

Suppose the FD controller is transmitting a 64B frame with data payload made of
0xFF bytes. The bus is then in \recessive{} state most of the time with only
\dominant{} stuff bits after every 5 \recessive{} bits. It is not hard to
imagine a scenario where the \dominant{} stuff bit (in data bitrate) falls out
of the sample time in nominal data bitrate for at least 11 nominal bit times
(see Figure \ref{fig:sja:ff}).

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/sja_ff}
    \cprotect\caption[Example of failing Bus Idle detection with sampling only
                      in nominal bitrate]
            {Example of failing Bus Idle detection with sampling only in nominal
            bitrate. \verb|sp_nom| is high in nominal bitrate sample point,
            \verb|sp_dat| in data bitrate sample point. \verb|sampled_nom| is
            low when in nominal sample point and low (\dominant{}) is sampled. On
            \verb|can_rx| are \recessive{} data with correct stuffing in data
            bitrate. Resolution to 100 ns time quanta, nominal bit time 16 tq
            (sp in $13/16 \approx 81.25\%$), data bit time 11 tq (sp in $9/11
            \approx 81.2\%$).}
    \label{fig:sja:ff}
\end{figure}

That way, the end of the FD frame is detected prematurely, and the rest of the
frame is treated as a new frame, which will most probably be malformed and
trigger an error condition. That in turn causes \textsc{Error Frame} to be sent
on the bus and the FD frame not to be received by any node, leading to
retransmission and perpetual errors (until the not-so-FD-tolerant node goes
\textit{error passive} or \textit{bus\_off}).

The solution is again simple. We do not know the data bitrate, but we do have
the undivided peripheral clock available. Instead of sampling the RX signal only
in sample time, it can be sampled at the undivided clock, checking for a
\textit{recessive-to-dominant} edge between this sample point and the preceding
one. This way the signal levels transitions on data bitrate are captured, and
the abovementioned problem is solved.

One potential drawback of this solution is making the controller more sensitive
to short-term glitches during the FD ignore period, but CAN FD controllers
default to single sampling during the bitrate-shifted data phase anyway.
Moreover, a \dominant{} glitch is much less likely than a \recessive{} glitch
(which is of no significance in this situation), as the \recessive{} level is
weaker. Better solutions require knowing (or guessing) the actual (or maximal)
data bitrate.

This situation may at first glance seem identical to that of a glitch to be
interpreted as SOF due to hard synchronization. However, while it is true that
the hard synchronization is performed on the \textit{recessive-to-dominant}
glitch, the SOF is sampled only in sample point -- and the bus is correctly
detected as \textsc{Idle} if the \dominant{} state is only a short glitch.


\section{Handling errors}

In the spirit of ``perform only minimal, surgical-like changes to the core so
that it works'', the initial design did not stop the main CAN FSM, and only
masked TX for the duration of the FD ignore period. This had some unpleasant
implications:
\begin{itemize}
\item The core likely goes into error state during an FD frame.
\item Error counters are increased.
\item When the FD ignore period ends, the core may still be ``transmitting''
      error frame.
\end{itemize}

The solution was simple -- the whole FSM must be blocked until the end of FD
ignore period. Due to the core design, this required changing every transition
condition.

\section{Clock skew}
\label{sec:sja:hardsync}
% hard sync in fdf_r
Another non-obvious problem occurs if the FD-tolerant receiver's and the FD
frame transmitter's clock are not synchronized. This is normally countered by
employing hard synchronization at SOF or resynchronization at every
\textit{recessive-to-dominant} edge. The problem is that during the data phase,
the bitrate may be shifted and is not in sync with the nominal bitrate.
Synchronization of the FD-tolerant receiver may theoretically be broken so that
it is shifted by half the nominal bitrate in the worst case. That poses a
problem if another frame immediately follows the FD frame. In an earlier
version, the end condition for the FD ignore period was the detection of 11
consecutive \recessive{} bits (in nominal bitrate), and only in the next bit
would the controller rejoin participation on the bus. If the drift is too large,
the SOF of the next frame might be received in place of the 11th bit of the
quiescent state, resulting in the FD ignore period to be extended over the
duration of the next frame.

This problem is not unique to this situation but might also happen in normal CAN
communication. The CAN standard, however, employs 2 counter-measures, which
completely prevent this problem:
\begin{itemize}
\item Hard synchronization on SOF
\item Transmitter must send SOF only after the 3 bits of \textsc{Intermission},
      but the receiver must interpret the SOF even in the last bit of
      \textsc{Intermission}.
\end{itemize}

The second step is already handled by not waiting for 11 \recessive{} bits, but
only for 8 and going to \textsc{Intermission}. However, to further minimize the
possible drift (and, admittedly, for historical reasons), hard synchronization
is performed on every \textit{recessive-to-dominant} edge during the FD ignore
period.

\section{Non-issues}

The \textsc{Suspend Transmission} field of \textsc{Interframe Space} is not an
issue, as it is relevant only if the node is currently a \textit{Transmitter},
but the node is capable of only \textit{receiving} FD frames\footnote{Neither
will the node \textit{transmit} an \textsc{Error Frame} in this situation.}.

\textsc{Arbitration} loss cannot occur when transmitting a frame simultaneously
with another node sending an FD frame. Even if the transmitted frame IDs were
identical for both nodes, the EDL bit indicating an FD frame is \recessive{}, so
the FD-tolerant (but not FD-capable) node would ``win'' with its non-FD frame
and cause \textsc{Bit-Error} for the FD-capable node, as the EDL bit is already
in \textsc{Control Field}, and not \textsc{Arbitration Field}.

\section{Stress-test: degenerate case}

Imagine the following stress-test:
\begin{itemize}
    \item Only one FD-capable device and one FD-tolerant SJA1000 are on the bus.
    \item The FD-capable device is sending a stream of FD frames with high CAN
          IDs (low priority), as tightly packed as possible.
    \item The FD-tolerant controller is sending a stream of CAN 2.0 frames with
    low CAN IDs (high priority). The frames should be packed tightly, but allow
    for the occasional transmission of the low-priority FD frame.
\end{itemize}

The FD-capable node is spamming FD frames, which should be ignored in the
FD-tolerant SJA1000. SJA1000 should also correctly step into the traffic and
send its own high-priority frames.

The waveforms in figures are captured by zlogan (see \fref{sec:extra:zlogan}) on
FPGA board. The CAN bus is routed internally, and all nodes are on the same
clocks. Nominal bitrate is 500K, data bitrate 5M. FD CAN ID 0x0FF, non-FD CAN
ID 0x0F0. Details may be seen in thesis source in \verb|figures/sja_crosstx|.

\subsection{Variant A: FD frames are acknowledged}
There is another FD-capable node on the bus, which ACKs the FD frames. Thus no
errors should occur anywhere on the bus, and all the frames from both
transmitters should be delivered.

\subsection{Variant B: FD frames are not acknowledged}
There are only the transmitting FD-capable node and the FD-tolerant SJA1000 on
the bus. Nobody ACKs the FD frames, which results in an error frame from the
transmitter. The non-FD frames should be delivered successfully.

As the FD-capable transmitter fails to receive \textsc{ACK} for its frame, it
sends an error frame and increments its \textsc{TX Error Counter} by 8
(\cite{spec:canfd}, Fault Confinement rule 3). The \textsc{TX Error Counter} has
no means to naturally decrease, and once it crosses 127, the node becomes
\textit{error passive}. From that moment, the \textsc{Acknowledgment-Error}
does not cause the increase of \textsc{TX Error Counter} (exception 2 from rule
3), but the \textsc{Bit Error} when the SJA starts to transmit its own frame in
the middle of \textsc{Error Delimiter} does. One of the nodes thus reaches
\textit{bus\_off} state, while the other stays \textit{error passive} and
continues to retransmit its frame forever.

The test has 3 phases:
\begin{enumerate}
    \item Both nodes are \textit{error active}.
    \item The FD-capable node becomes \textit{error passive}. SJA1000 remains
          \textit{error active}.
    \item SJA1000 goes \textit{error passive}. Both nodes are \textit{error
          passive}.
\end{enumerate}

\subsubsection{Phase 1}

The FD-capable controller is transmitting a frame and does not receive
\textsc{Ack}. After the \textsc{Ack Slot}, it sends an \textsc{Active Error
Flag}, followed by 8 \recessive{} bits of \textsc{Error Delimiter} and 3
\recessive{} bits of \textsc{Intermission}.

The FD-tolerant SJA1000 is in FD ignore mode and remains there for the
duration of the whole \textsc{Error Frame}. After the 8 \recessive{} bits of
\textsc{Error Delimiter}, the FD ignore period ends, and the core goes to
\textsc{Intermission}. Both nodes remain in sync.

The non-FD frames from SJA1000 are all delivered successfully.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/sjax_ph1}
    \caption[SJA cross TX test, Phase 1]
        {SJA cross TX test, Phase 1. Both the SJA and FD-capable (in this case
        CTU CAN FD) nodes are \textit{error active}.}
    \label{fig:sja:crosstx:ph1}
\end{figure}

\subsubsection{Phase 2}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/sjaxt_fdf}
    \caption[SJA cross TX test, model for non-acknowledged FD frame]
        {SJA cross TX test, model for non-acknowledged FD frame. The FD-capable
        node is \textit{error passive}. The SJA node may be either \textit{error
        active} (Phase~2), in which case it does not transmit the
        \textsc{Suspend Transmission} field and may transmit SOF immediately
        after its own \textsc{Intermission} (blue lines), or \textit{error
        passive} (Phase~3; red lines). In variant (a), the last bit of
        \textsc{CRC Sequence} is dominant\textsuperscript{a}. Variant (b) shows
        the other extreme when the last 4 bits are
        recessive\textsuperscript{b}.\\
        {\footnotesize \textsuperscript{a} Or more precisely, there has
        been a \textit{recessive-to-dominant} edge in the nominal bit time
        preceding \textsc{CRC Delimiter}.}\\
        {\footnotesize \textsuperscript{b} FD frames use fixed
        stuffing. The exact reasoning why this is the maximum is given in
        \fref{subsec:sja:degen:mitigations}}
        }
    \label{fig:sja:crosstx:model:fdf}
\end{figure}

When the FD controller goes \textit{error passive}, the situation changes. There
is no more the synchronizing \textsc{Active Error Flag}, and the controllers go
off sync. As can be seen in \ref{fig:sja:crosstx:model:fdf}, the FD ignore
period ends in the middle of the FD-capable controller's \textsc{Error
Delimiter}, and so does all of SJA's \textsc{Intermission}.

SJA then starts its own transmission, and since the other controller is not
ready for it, it will not be acknowledged -- the FD-capable node instead starts
to transmit a \textsc{Passive Error Frame}, which will not be detected by the
SJA. That causes the SJA to send an \textsc{Active Error Frame}, which again
synchronizes the two nodes.

The retransmitted frame from SJA should now be delivered successfully. After an
FD frame, the situation repeats, and one non-FD frame is sent non-acknowledged,
leading to \textsc{Error Frame}, increase of \textsc{TX Error Counter}, and then
successful retransmission.

The situation changes again when the SJA becomes \textit{error passive}.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/sjax_ph2}
    \caption[SJA cross TX test, Phase~2]
        {SJA cross TX test, Phase~2. The CTU CAN FD node is \textit{error
        passive}, while the SJA remains \textit{error active}.}
    \label{fig:sja:crosstx:ph2}
\end{figure}

\subsubsection{Phase 3}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/sjaxt_sjaf}
    \caption[SJA cross TX test, model for non-acknowledged SJA frame]
        {SJA cross TX test, model for non-acknowledged SJA frame. Both nodes
        are \textit{error passive} (Phase 3). In variant (a), the last bit of
        \textsc{CRC Sequence} is dominant. Variant (b) shows the other extreme
        when the last 4 bits are recessive. Blue lines indicate the start of
        retransmission in conformant case, red lines represent the case when the
        FD node does not send \textsc{Suspend Transmission}.}
    \label{fig:sja:crosstx:model:sjaf}
\end{figure}

Now both nodes are \textit{error passive}, and no form of forced synchronization
may occur, except for the bus being \textsc{Idle} long enough for both nodes to
become \textsc{Idle}. However, as this is a stress test, there is no such
luxury.

When an FD frame is being transmitted, the situation starts as in the
previous phase. SJA starts sending its own frame in the middle of the other's
node \textsc{Passive Error Frame}, resulting in SJA
\textsc{Acknowledgment-Error} when no acknowledgment is received for the frame.

However, now the SJA sends a \textsc{Passive Error Flag}. The FD-capable core
has been transmitting its own \textsc{Passive Error Flag} at the
time\footnote{The error passive station waits for six consecutive bits of equal
polarity, beginning at the start of the \textsc{Passive Error Flag}. The
\textsc{Passive Error Flag} is complete when these 6 equal bits have been
detected. (\cite{spec:canfd}, 3.2.3)}.
It depends on the frame transmitted by SJA when the \textsc{Passive Error Flag}
of the FD-capable node will end. Anything between two border cases may occur --
either the last bit of \textsc{CRC Sequence} field is \dominant{}, or the 4 last
bits of it are \recessive{}\footnote{If 5 last bits were \recessive{}, a
\dominant{} stuff bit would follow.}.

In all cases, the \textit{Transmitter} SJA node will reach \textsc{Idle} in the
FD-capable node's \textsc{Suspend Transmission}, as indicated by the blue lines
in \fref{fig:sja:crosstx:model:sjaf}. The FD-capable node now attempts to
retransmit its FD frame. As the SJA is still in \textsc{Suspend Transmission},
it does not participate in arbitration. The FD frame is not acknowledged, the
FD-capable node sends \textsc{Passive Error Frame}, into which the SJA starts to
send its own frame and the whole cycle repeats. From this point onwards, no
frame will be delivered\footnote{As all the frames are retransmissions, there is
no option from SW to delay sending the frames.}.

\Fref{fig:sja:crosstx:ph3} shows a different outcome. At the time of writing,
CTU CAN FD does not transmit \textsc{Suspend Transmission} after the
\textsc{Passive Error Frame}, and starts the retransmission
sooner\footnote{Submitted as issue \#225.}. As is evident from the red lines in
\fref{fig:sja:crosstx:model:sjaf}, two distinct situations may now occur:
\begin{itemize}
    \item The FD-capable node starts the retransmission in the first two bits
          of SJA's \textsc{Intermission}, which is interpreted as an
          \textsc{Overload Frame}. That causes the nodes to synchronize again,
          and they will both participate in the next arbitration.
    \item The retransmission starts in SJA's \textsc{Error Delimiter}, causing
          it to start a new \textsc{Passive Error Frame}. This is exactly what
          happened before to the FD-capable controller -- their roles are now
          swapped. The SJA does transmit \textsc{Suspend Transmission}, and the
          FD node will start its next retransmission in this period. The SJA
          detects the FD frame, and the cycle starts anew.
\end{itemize}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/sjax_ph3_overload}
    \caption[SJA cross TX test, Phase 3]
        {SJA cross TX test, Phase 3. Both nodes are now \textit{error passive}.
        The captured waveform differs from the theoretical prediction because
        CTU CAN FD fails to transmit \textsc{Suspend Transmission} after the
        \textsc{Passive Error Frame}.}
    \label{fig:sja:crosstx:ph3}
\end{figure}


\subsection{Possible mitigations}
\label{subsec:sja:degen:mitigations}

First of all, let it be noted that this whole situation happens only under very
specific conditions -- there is no node to acknowledge the FD frames, and there
is no other node (except the FD-capable one) to acknowledge the non-FD frames.
That is not how real-world buses are likely to look. A slightly more likely case
is that all other nodes go \textit{bus\_off}. But in that case, the bus is in a
bad condition anyway, so this does not add too much of a risk. Moreover,
as soon as another node reintegrates, it will ACK one of the frames and the
endless cycle will break.

In any case, one way to mitigate this issue is to add some wait states after
ignoring an FD frame, as the initial cause of the problem is the SJA starting to
transmit in the middle of \textsc{Passive Error Frame}.
% TODO: check above .. when th is sent in error delimiter, it is detected as
%       Bit-Error and another error frame is send ... and this time, the
%       passive error flag is extended until 6 consecutive recessive (or
%       dominant) bits are monitored
The simplest way is for the node also to transmit \textsc{Suspend Transmission}
field of \textsc{Interframe Space} if the node was \textit{Receiver} and did
just ignore an FD frame. However, this would introduce a new set of problems.
Calling the new field \textsc{After-FD Suspend Transmission}, here are the
requirements for it (and its differences from \textsc{Suspend Transmission} at
the same time):
\begin{itemize}
    \item The time to wait has to be 8 + \textit{however many end bits of the
          FD frame CRC may be recessive}. In FD frames, \textsc{CRC Sequence} is
          stuffed using fixed bit stuffing -- a stuff bit is inserted at the
          beginning and after every 4 bits. Let us suppose that the last fixed
          stuff bit is \recessive{}, and the remaining bits of \textsc{CRC
          Sequence} are also \recessive{}. For CRC\_15, CRC\_17, and CRC\_21,
          the number of these bits is 3, 1, 1, respectively. The
          \textsc{After-FD Suspend Transmission} field thus needs to be 12 bits
          long.
    \item It should not unduly delay higher-priority frames. Thus the node
          transmitting \textsc{After-FD Suspend Transmission} should participate
          in arbitration if another node starts transmitting during the period.
          It will not, however, initiate with \textsc{SOF} on its own until it
          finishes \textsc{After-FD Suspend Transmission}.
\end{itemize}

This mitigation would at worst cause 12bit delay of a frame directly following
an FD frame, in a situation that no other node wants to transmit at the time. At
best, it can prevent a ``lockup'' in one engineered pathologic situation.

This mitigation is \textit{not} implemented in the extended FD-tolerant SJA1000
controller.

\subsection{Other findings}

This contrived test helped discover a bug in the original SJA1000 core (see next
section), a bug in the CTU CAN FD controller (see Figure
\ref{fig:sja:crosstx:ph3}), and an issue with the SJA's FD tolerance
implementation itself\footnote{As described earlier, the problem was with
detecting the end of the FD ignore period for data frames, where the \textsc{CRC
Delimiter} field was ignored. As the result, the core resumed bus participation
one bit time late.}. At the time of writing, the CTU CAN FD issue has an open
gitlab issue, and the other bugs are fixed.

\section{Perpetual reset bug}

A rather serious bug was discovered in the SJA1000 core. When SJA1000 goes to
\textit{bus\_off}, it switches to Reset mode \cite{doc:sja}. When the driver
attempts to reset the controller and re-enable it by requesting to leave the
Reset mode, the controller first waits for 128 occurrences of 11 consecutive
\recessive{} bits, as per specification \cite{spec:can20}. However, due to
internal signal duplication and unmatched conditions, the core remained forever
in Reset mode. This was fixed in commit 02c7660645fb and further improved in
8e13b92c361d.

\begin{comment}
Spec:
Also after a transmission, an error passive unit will wait before initiating a further transmission. (See SUSPEND TRANSMISSION)

Ignoring an FD frame should be considered as sending an EP error frame for the purposes of Suspend. So, if the node is Error Passive, it should additionally suspend its transmission after successfully ignoring an FD frame.


The error passive station waits for six consecutive bits of equal
polarity, beginning at the start of the PASSIVE ERROR FLAG. The PASSIVE ERROR FLAG is complete when these 6 equal bits have been detected. (3.2.3)

After transmission of an ERROR FLAG each station sends recessive bits and monitors the bus until it detects a recessive bit. Afterwards it starts transmitting seven more recessive bits. (3.2.3)



In order to terminate an ERROR FRAME correctly, an error passive node may need the bus to be BUS IDLE for at least 3 bit times (if there is a local error at an error passive Receiver). Therefore the bus should not be loaded to 100%. (3.2.3)


\end{comment}





% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\endinput
a) Modify SJA1000 soft IP core from OpenCores to tolerate CAN FD traffic on the bus.
- SJA1000 FD Tolerant
    - Motivation: take what works and make it FD tolerant
    - Background: SJA1000, OpenCores implementation
        - Integrating: Core, APB/AXI, device tree
    + Idea
        - when EDL bit is recessive (FD frame), wait out the frame
        - wait for EOF + interframe or error delimiter + interframe
        - completely disable TX for the node for the duration of the FD frame
            - EF, ACK
        - Idea 1:
            - it does not matter if the core enters error mode, because TX is gated
            - unfortunately, it matters when the bus is under high load (back-to-back frames) -> error state must not be entered
    + hitch
        - looking at nominal-bitrate samplepoints is not enough
        - when sending FD frame with a lot of 1's, it was misdetected as EOF and error frame was generated
        - a really simple form of BRS is employed - there is recessive-to-dominant edge detector on fast clock, which is reset at each sample point. That way, no dominant bit on data bitrate is missed and the EOF is detected correctly.
        - possible disadvantage - glitches, but common mode is filtered out by physical layer (twisted pair + common-mode choke) and when the bus is idle, there cannot be glitches anyway (it would trigger hard sync). So it is less a problem.
    + hitch 2
        - weird situations when only 2 devices on the bus (SJA1000-fdtol and FD device). On 100\% bus load and both ends transmitting (FD vs CAN 2.0),
        both devices go over error passive to bus-off. No frame is delivered correctly (though the classical frames should).
        -> This helped discover a bug in the original core - when the controller
        got to bus-off, it entered reset mode and it was impossible to leave it
        except for resetting the whole board.
        - many months later, it was nearly accidentally discovered that this problem may be due to incorrect waiting time for the bus to be idle (issue #1 for sja1000-fdtol)
