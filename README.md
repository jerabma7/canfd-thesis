# Open-source and Open-hardware CAN FD Protocol Support

a.k.a. my Master's Thesis.

The PDF for the latest version (on the master branch): [Jerabek-thesis-2019-canfd.pdf](https://gitlab.fel.cvut.cz/jerabma7/canfd-thesis/-/jobs/artifacts/master/file/Jerabek-thesis-2019-canfd.pdf?job=build)

Defense presentation (in Czech): [prezentace.pdf](https://gitlab.fel.cvut.cz/jerabma7/canfd-thesis/-/jobs/artifacts/master/file/prezentace/prezentace.pdf?job=build)

For older or other versions, you may download the PDF on the [pipelines page](https://gitlab.fel.cvut.cz/jerabma7/canfd-thesis/pipelines).


## Requirements

* pdflatex
* python and pygmentize (for minted)
* pygments-github-lexers (pip; for highlighting TOML)
* DtsLexer for Pygments (included)
* make
* bibtex
* dot (for rendering diagrams)
* inkscape (for exporting SVGs to PDFs)

Not used:
* ImageMagick (for exporting bitmap images)
* ditaa (for rendering TXT images)
* wavedrom-cli (for rendering images of bus transactions)
