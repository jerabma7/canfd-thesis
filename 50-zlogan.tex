\chapter{Extra work}
\label{ch:extra}

\section{Extending zlogan}
\label{sec:extra:zlogan}

Zlogan\footnote{Available at \url{https://github.com/eltvor/zlogan}} is an
in-chip logical analyzer originally developed by Marek Peca. It captures its
input signals and streams all changes via AXI-Stream to a FIFO, from where it is
read by DMA to an application and saved into a file.

The analyzer has been an essential component during testing both the modified
SJA1000 and CTU CAN FD in hardware.

\subsection{Modifications}

The base operation remains unchanged, but the following changes had to be made:
\begin{itemize}
\item Encapsulate zlogan as Vivado IP.
\item Create register map and APB interface.
\item Fix the IP to be able to handle a (theoretically) arbitrary number of
      input signals.
\item Extend the streaming application to
    \begin{itemize}
    \item Allow for reading more data than the DMA engine's maximum transfer
          size.
    \item Allow to abort the stream while saving already transferred data.
    \item Detect and report errors.
    \item Use a portable way to allocate buffer for DMA (via \verb|udmabuf|
          driver).
    \end{itemize}
\end{itemize}

The updated version may be found in the original github repository in branch
\verb|zlogan-component|.

At the time of writing, there are several (unstable) changes in a development
branch, including:
\begin{itemize}
    \item Separating parts of the IP into different entities
    \item Creating unit tests for the core entities
    \item Correctly resetting registers on zlogan enable and flushing all
          buffers at disable.
\end{itemize}

\subsection{Usage}

The \textit{zlogan} IP requires additional components (on Zynq):
\begin{itemize}
    \item AXI DMA
    \item AXI4-Stream Data FIFO
    \item AXI-APB Bridge
\end{itemize}
An example top-level design (except the actual analyzer inputs) is depicted in
\fref{fig:zlogan:design}. At the moment, the whole design (from \verb|la_inp| to
AXIS FIFO) is synchronous to one clock. If sampling on higher frequency is
desired, the AXIS FIFO may be made asynchronous, and the whole core may operate
on a higher frequency than PS's AXI.

% TODO: figure: internal schema + ref

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/zlogan_design2}
    %\includegraphics[height=\textheight]{figures/zlogan_design2}
    \caption{Example of top-level design with zlogan in Vivado}
    \label{fig:zlogan:design}
\end{figure}

On the SW side, at the moment everything runs in userspace. In the \verb|rxla|
program, modify the config file \verb|hw_config.h| and set zlogan registers
address, AXIS DMA registers address, and width of the DMA transfer length
register\footnote{It is recommended to set it as high as possible/reasonable in
the DMA IP configuration. While the stream is fetched using multiple DMA
transfers if needed, there might be some glitches on transfer boundaries.}. Now
it is ready to be compiled. In addition, the \textit{udmabuf} module has to be
compiled and loaded, to provide a memory region safe for DMA transfers to
userspace.

The \verb|rxla| application transfers data from the analyzer and saves them into
a file, until either the given maximum number of bytes is transferred or the
program is interrupted (via Ctrl+C).


\section{Rewrite of CAN crossbar IP}
\label{sec:extra:canxbar}

During the testing, it was necessary to test the various CAN controllers
connected to either an external CAN bus or more simply to an in-chip internal
bus. To avoid having to rebuild the whole FPGA image for every possible
configuration, the \verb|can_crossbar| IP exists.

The basic idea is that there are $N$ in-chip \textit{controllers} and $M$
external \textit{buses}. Each of these may be connected to exactly one of $L$
\textit{lines}, which facilitate the N:M mapping of controllers to buses.
Multiple buses or controllers may be attached to each line, the driving signals
merged by logical AND.

While the \verb|can_crossbar| IP was already available from my Bachelor's
thesis \cite{bakalarka}, it was not tested and even I, the author of the IP,
doubted about its functionality (and didn't like its naive structure). Since it
is a very simple IP, it was decided to rewrite it, this time properly and with
testbenches\footnote{The old IP may be found in the toplevel design repository
at \path{/system/ip/can_crossbar_1.0}, the new one at
\path{/system/ip/can_crossbar_2.0}}.

The structure is simple. For each line, merge all unmasked sources as its input,
and connect each sink to its selected line. RX and TX directions are handled
independently, so there are independent RX and TX channels of a line. The
masking is done by demultiplexers with non-selected outputs driven to logical 1
(\recessive{} state). An example of this structure may be seen in
\fref{fig:canxbar:lines}.

\begin{figure}
    \centering
    \begin{subfigure}[t]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/can_xbar_half}
        \caption{One half of \textit{can\_crossbar}, instantiated with 4
                 \textit{sources}, 3 \textit{sinks} and 2 \textit{lines}}
        \label{fig:canxbar:lines}
    \end{subfigure}
    ~%
    \noindent
    \begin{subfigure}[t]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/can_xbar_oe}
        \caption{Output Enable function}
        \label{fig:canxbar:oe}
    \end{subfigure}
    \caption{Structure of \textit{can\_crossbar}}
    \label{fig:canxbar:impl}
\end{figure}



Lastly, each \textit{line} may either be connected to external buses (which are
assigned to the line) or looped into itself (line's TX channel is connected to
its RX channel), in which case the TX channel of the assigned external buses is
driven high.

The new IP interfaces the system via APB instead of the much more heavy-weight
AXI-Lite. The number of \textit{controllers}, \textit{buses}, and \textit{lines}
is configurable for the underlying component, but it is fixed for the whole IP
as to simplify the register map design.

\subsection{Driver}

There is no Linux driver for the IP. Its register map may be accessed either
directly via \path{/dev/kmem} (and command-line utility \verb|devmem|) or via
Userspace I/O driver (UIO). The UIO driver takes information about the register
map from device tree and offers applications to \verb|mmap(2)| the
registers into the application's address space. \cite{linux:uio,uio}

\subsection{Register Overview}

All registers are 32bits wide and should only be accessed by 32bit words.

%\vfil\break

\subsubsection{CAN Configuration Register}\leavevmode\\

\noindent
Address offset: 0x000\\
Reset value: 0x00000000\\


\begin{register}{!ht}{CCR}{0x000}
	\label{CAN Configuration Register}%
	\regfield{Reserved}{4}{28}{0}
	\regfield{OE\_LINE4}{1}{27}{0}%
	\regfield{OE\_LINE3}{1}{26}{0}%
	\regfield{OE\_LINE2}{1}{25}{0}%
	\regfield{OE\_LINE1}{1}{24}{0}%
	%\regnewline%
	\regfield{BUS4\_LINE}{2}{22}{00}%
	\regfield{BUS3\_LINE}{2}{20}{00}%
	\regfield{BUS2\_LINE}{2}{18}{00}%
	\regfield{BUS1\_LINE}{2}{16}{00}%
    \regfield{CTRL8\_LINE}{2}{14}{00}%
    \regfield{CTRL7\_LINE}{2}{12}{00}%
    \regfield{CTRL6\_LINE}{2}{10}{00}%
    \regfield{CTRL5\_LINE}{2}{ 8}{00}%
	\regfield{CTRL4\_LINE}{2}{ 6}{00}%
	\regfield{CTRL3\_LINE}{2}{ 4}{00}%
	\regfield{CTRL2\_LINE}{2}{ 2}{00}%
	\regfield{CTRL1\_LINE}{2}{ 0}{00}%
	\reglabel{Reset}\regnewline%
	\begin{regdesc}
	\begin{reglist}[CANn\_LINE]
		\item [OE\_LINEn] LINEn Output Enable. If set to 1, the line is
                          connected to its assigned external bus.
                          If set to 0, the line TX signal is looped to its
                          RX signal, and the external bus is disconnected.
                          This effectively connects all CAN controllers attached
                          to the line together.

		\item [BUSn\_LINE] Specifies which line is the external bus
        connected to:\\
        00: The external bus is connected to LINE1\\
        01: The external bus is connected to LINE2\\
        10: The external bus is connected to LINE3\\
        11: The external bus is connected to LINE4

		\item [CTRLn\_LINE] Specifies which line is the CAN controller
        connected to:\\
        00: The CAN Controller is connected to LINE1\\
        01: The CAN Controller is connected to LINE2\\
        10: The CAN Controller is connected to LINE3\\
        11: The CAN Controller is connected to LINE4
	\end{reglist}
	\end{regdesc}
\end{register}
