from setuptools import setup, find_packages

setup (
  name='DtsLexer',
#  packages=find_packages(),
  py_modules=['DtsLexer'],
  entry_points =
  """
  [pygments.lexers]
  dstlexer = DtsLexer:DtsLexer
  """,
)
